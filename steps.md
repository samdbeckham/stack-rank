# Stack Rank

## Graph QL

The following query fetches the data for the issues on a project, along with the name of a board and its lists.
This is useful for showing the stack-ranked list, but not necessary.

```graphql
query board {
  project(fullPath: "samdbeckham/test") {
    board(id: "gid://gitlab/Board/3629263") {
      name
      lists {
        nodes {
          id
        }
      }
    }
    issues(state: opened) {
      nodes {
        id
        iid
        relativePosition
      }
    }
  }
}
```

This is the mutation we want.
The way we pass things is a bit odd, but it's kinda intuitive for how we use boards.
You tell it what list to look at, then tell it to move a certain issue above or below another issue on that list.
You can use this mutation to move issues on to other lists too, but we don't need that for this app.

```graphql
mutation moveIssue {
  issueMoveList(input: {
    boardId: "gid://gitlab/Board/3629263",
    projectPath: "samdbeckham/test",
    iid: "11",
    fromListId: 10359698,
    toListId: 10359698,
    moveBeforeId: 98739332
  }) {
    issue {
      relativePosition
    }
  }
}
```

Whilst this approach works great for moving issues on boards, it's not great for maintaining a stack rank.
In order to maintain a stack rank we'd need to:

1. Fetch an array of all the issues in that list (probably ~workflow::ready-for-dev)
2. Get the ID of the issue at the top of that list
3. Loop through each issue in our stored stack rank and run the following logic:
  1. If the current issue does not match the top issue, place it *above*
  2. If the issue matches the top issue, do *nothing*
  3. For every subsequent issue, place it *below* the previous one.

For example, consider the following list and stack rank:

```javascript
const stackRank = ["one", "two", "three", "four", "five"];
const currentList = ["three", "one", "five", "two", "four"];
```

The top issue currently is `"three"`.
When looping through the stack rank, we start at issue, `"one"`.
Since these do not match, we fire a mutation to place `"one"` above `"three"`.
The list now looks like this:

```javascript
let filteredList = ["one", "three", "five", "two", "four"];
```

The next issue in the loop is, `"two"`.
Again, this does not match our top issue, `"three"`.
_Note: we don't refresh the top issue, it stays as whatever it was at the start of the loop._
We place `"two"` above `"three"` and get this:

```javascript
filteredList = ["one", "two", "three", "five", "four"];
```

The next issue in the loop is `"three"`.
This matches our stored top issue, so we do absolutely nothing.
The list looks the same but now we store a new variable, `lastIssue` and set it to `"three"`.

```javascript
let lastIssue = "three";
```

The next issue in the loop is `"four"`.
Since we've already matched our top issue, we move into the next phase and place this issue directly after our last issue.
In the previous step, we set that to `"three"`.
So we fire a mutation to move `"four"` after `"three"` and set `lastIssue` to `"four"`.
Our list now looks like this:

```javascript
lastIssue = "four";
filteredList = ["one", "two", "three", "four", "five"];
```

The list is fully in order now, but we have one step left.
There may be an optimization we can run at this point to break out of the loop if the list is in order, but for clarity we'll keep going.

The final issue in our loop is, `"five"`.
We follow the same steps as the previous one and place this issue directly after our last one.
Since we updated `lastIssue` to equal, `"four"`, we fire a mutation to place `"five"` after `"four"`.

That's the end of the list!
We're now fully sorted.

## Storage

Since the point of this app is to set the stack rank, we need a way to store the changes that are made to the stack rank.
Databases are a nightmare and IHNIWID, so I ideally want to store this in a JSON file.
This could be as simple storing an array of `IID`s like this:

```json
{
  "stack_rank": ["11", "22", "15", "17", "9"]
}
```

We can store this in the repo itself and use it to seed the app.
When the list is updated and "saved", we can do a commit behind the scenes to this file.
We'd need to authenticate the user on the app but that's simple enough and it allows us to gate-keep who can access it too.
We'd probably need to do this for the mutations anyway.

This gives us version history too which is nice.
We may even be able to get smart with branches so people can save their local configs without messing up the live stack rank.

We'd likely have to fetch the JSON from the repo each time to be sure we're up to date and not having to wait ~10 minutes for the pipeline to run.
That kinda gives us an isomorphic feel.

## Pipeline

Nice and short, we need a scheduled pipeline to run that keeps the list in order.
We just need to follow the steps in the previous section.
This same pipeline should run on a schedule and whenever `stack_rank.json` is updated.

## Unknowns

Lists is the biggest roadblock I can see at the moment.
We need to pass the mutation a list ID on a board.
We could use the `workflow::ready for dev` list but I don't know if that would cause problems if issues move in or out of the list.
It may be better to have a "secret" board with a full list of all the issues in certain workflows.
That would allow us to maintain the stack rank when issues move between the different labels.

There may be a problem is an issue is stored in the stack rank but doesn't exist any more.
Either because it was closed or it moved onto another list outside our scope.