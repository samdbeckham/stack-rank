import stackRankData from "../data/stack_rank.json" assert {type: "json"};
import fetch from "node-fetch";

const {
  GITLAB_TOKEN,
  VUE_APP_GL_BOARD_ID,
  VUE_APP_GL_LIST_ID,
  VUE_APP_GL_PROJECT,
} = process.env;

const ABOVE = "ABOVE";
const BELOW = "BELOW";

const GQL_API = "https://gitlab.com/api/graphql";
const GQL_OPTIONS = {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${GITLAB_TOKEN}`,
  },
};

async function fetchIssues() {
  const body = JSON.stringify({
    query: `query board {
      project(fullPath: "${VUE_APP_GL_PROJECT}") {
        board(id: "${VUE_APP_GL_BOARD_ID}") {
          lists(id: "${VUE_APP_GL_LIST_ID}") {
            nodes {
              issues {
                nodes {
                  iid
                  id
                }
              }
            }
          }
        }
      }
    }`,
  });

  const raw = await fetch(GQL_API, { ...GQL_OPTIONS, body });
  const { data, errors } = await raw.json();

  if (errors) {
    console.error(errors);
  }

  return errors || data.project.board.lists.nodes[0].issues.nodes;
}

async function place(issue, dest, target) {
  console.log(issue.iid, dest, target.iid);
  const body = JSON.stringify({
    query: `mutation moveIssue {
      issueMoveList(input: {
        boardId: "${VUE_APP_GL_BOARD_ID}",
        projectPath: "${VUE_APP_GL_PROJECT}",
        iid: "${issue.iid}",
        fromListId: "${VUE_APP_GL_LIST_ID}",
        toListId: "${VUE_APP_GL_LIST_ID}",
        ${dest === BELOW ? "moveBeforeId" : "moveAfterId"}: ${target.id.replace(
      "gid://gitlab/Issue/",
      ""
    )}
      }) {
        issue {
          iid
          relativePosition
        }
      }
    }`,
  });

  const raw = await fetch(GQL_API, { ...GQL_OPTIONS, body });
  const { data, errors } = await raw.json();

  if (errors) {
    console.error(errors);
  }

  return errors || data;
}

const currentIssues = await fetchIssues();
const stackRank = stackRankData["stack_rank"];

let firstIssuePassed = false;
let currentIssue = currentIssues[0];

for (const issue of stackRank) {
  let result;
  if (issue.iid === currentIssue.iid) {
    firstIssuePassed = true;
    currentIssue = issue;
    console.log(currentIssue.iid, "stays");
  } else {
    if (firstIssuePassed) {
      result = await place(issue, BELOW, currentIssue);
      currentIssue = issue;
    } else {
      result = await place(issue, ABOVE, currentIssue);
    }
  }
}
